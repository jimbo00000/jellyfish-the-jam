# OpenGL Sketches in Lua #

## TODO List ##

### Framework ###

Embed interpreted scenes in compiled app
  - Sometimes this results in odd OpenGL errors
  - Is it legal to call into an already created context with GL ffi bindings?

Rewrite tracks to use dense array with O(n) inserts a la rocket
  - and keep a table of names for track lookups
  - or update each track on every frame with O(1) cost per kf

Lua
  - Better Lua oo-style throughout  o:function()
  - Prototype-style inheritance
  - Find out if I'm doing something silly with memory allocations
     - Why isn't the GC running?

Unify VAOs in spline_scene? What's best?
Draw all 8 tentacles as one instanced draw?
Store all geometry in one buffer so one jellyfish = one instance?
