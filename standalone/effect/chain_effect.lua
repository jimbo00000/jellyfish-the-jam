-- chain_effect.lua
-- A two-pass effect, chaining the results of 2 buffers

chain_effect = {}

local openGL = require("opengl")
local ffi = require("ffi")
local fbf = require("util.fbofunctions")

local blur_effect = require("effect.twopassblur_effect")
local edge_effect = require("effect.onepassblur_effect")

function chain_effect.initGL()
    blur_effect.initGL()
    edge_effect.initGL()
end

function chain_effect.exitGL()
    blur_effect.exitGL()
    edge_effect.exitGL()
end

function chain_effect.bind_fbo()
    edge_effect.bind_fbo()
end

function chain_effect.unbind_fbo()
    fbf.unbind_fbo()
end

function chain_effect.resize_fbo(w,h)
    edge_effect.resize_fbo(w, h)
    blur_effect.resize_fbo(w, h)
end

function chain_effect.present(win_w, win_h)
    blur_effect.bind_fbo()
    edge_effect.present(win_w, win_h)
    fbf.unbind_fbo()
    blur_effect.present(win_w, win_h)
end

return chain_effect
