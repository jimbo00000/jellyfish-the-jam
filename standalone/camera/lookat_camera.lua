--[[
    lookat_camera.lua

    A simple camera with location that looks at a static target point
    and always points to positive y up.
]]
lookat_camera = {}

local mm = require("util.matrixmath")

local cam = {
    loc = {20,10,-40}, -- x is up here
    target = {0,0,0},
    up = {0,1,0},
}
lookat_camera.params = cam

function lookat_camera.cammove(dp)
    local c = cam.loc
    for i=1,3 do
        c[i] = c[i]+dp[i]
    end
end

function lookat_camera.targetmove(dp)
    local c = cam.target
    for i=1,3 do
        c[i] = c[i]+dp[i]
    end
end

function lookat_camera.get_matrix()
    local m = {}
    mm.glh_lookat(m, cam.loc, cam.target, cam.up)
    return m
end

--[[
function make_modelview_matrix()
    local campos = {
        camera.loc[1],
        camera.loc[2],
        camera.loc[3]
    }

    local m = {}
    mm.make_identity_matrix(m)
    mm.glh_rotate(m, camera.pitch, 1,0,0)
    mm.glh_rotate(m, camera.roll, 0,0,1)
    mm.glh_rotate(m, camera.yaw, 0,1,0)
    mm.glh_translate(m, campos[1], campos[2], campos[3])
    mm.glh_rotate(m, .5*math.pi, 0,0,1)
    return m
end
]]

return lookat_camera
