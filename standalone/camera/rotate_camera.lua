--[[
    rotate_camera.lua

    A simple camera that rotates around a focus point and always points up.
]]
rotate_camera = {}

local mm = require("util.matrixmath")

local cam = {
    radius = 40,
    theta = 0,
    height = 12,
    up = {0,1,0},
}
rotate_camera.params = cam

function rotate_camera.cammove(dp)
    cam.radius = cam.radius + dp[3]
    cam.theta = cam.theta + .03 * dp[1]
    cam.height = cam.height + dp[2]
end

function rotate_camera.get_matrix()
    local m = {}
    local loc = {cam.radius * math.sin(cam.theta), cam.height, cam.radius * math.cos(cam.theta)}
    local target = {0,0,0}
    mm.glh_lookat(m, loc, target, cam.up)
    return m
end

return rotate_camera
