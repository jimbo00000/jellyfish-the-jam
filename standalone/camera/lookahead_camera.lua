--[[
    lookahead_camera.lua

    A simple camera with location that looks forward and always points to positive y up.
]]
lookahead_camera = {}

local mm = require("util.matrixmath")

local cam = {
    loc = {0,5,40}, -- x is up here
    up = {0,1,0},
    yaw = 0,
}
lookahead_camera.params = cam

function lookahead_camera.cammove(dp)
    local c = cam.loc
    for i=1,3 do
        c[i] = c[i]+dp[i]
    end
end

function lookahead_camera.camyaw(d)
    cam.yaw = cam.yaw - d
end

function lookahead_camera.get_matrix()
    local m = {}
    local target = {}
    local fwd = {math.sin(cam.yaw), 0, math.cos(cam.yaw),}
    for i=1,3 do
        target[i] = cam.loc[i] - fwd[i]
    end
    mm.glh_lookat(m, cam.loc, target, cam.up)
    return m
end

return lookahead_camera
