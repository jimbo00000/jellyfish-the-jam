# standalone #

## Purpose ##
This directory was originally intended as a harness for testing lua "Scenes", which contain internal state and the functions:  
 - initGL  
 - timestep  
 - renderForOneEye(mvmtx, prmtx)

The idea was to serialize all the code for a scene and send it over the network to a running instance of a graphical VR app, where it could be loaded and run without quitting said app.

It basically worked... more detailed testing is needed.

## Usage ##

### Mainapp graphical test bed ###

Drag ***mainapp.lua*** onto ***luajit.exe***. Different key shortcuts will modify graphical parameters of the given scene. The scene can be changed with the 1,2,3... keys and the post-effect can be changed with the F1, F2, F3 keys.

### Rocket demo proof-of-concept ###

Launch ***luajit.exe rocketdemoinlua.lua*** with the first param 'sync' to connect to a running instance of the rocket editor (see [emoon's rocket](https://github.com/emoon/rocket)). Running without any parameters the demo will attempt to load its keyframes from a file called ***tracks.lua***, poreviously exported by the app itself via a command from the rocket editor.


