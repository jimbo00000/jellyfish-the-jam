--[[
	rocketeditorinlua.lua

	A tiny example to simulate only the barest minimum of GNU rocket editor functions:
	the pause/resume command.
	- luajit.exe rocketeditorinlua.lua
	- Launch demo with 'sync' parameter
	- Press space in this editor window to toggle pause

	NOTE: this is broken, app exits after first pause.
]]

local ffi = require( "ffi" )
package.loadlib("socket/core.dll", "luaopen_socket_core")
local socket = require("socket.core")
local rk = require("rocket")

local glfw = require( "GLFW" )
local openGL = require( "opengl" )
openGL.loader = glfw.glfw.GetProcAddress
openGL:import()

io.stdout:setvbuf("no")
local client = rk.connect_editor()

function init_gl()
end

function onkey(window,k,code,action,mods)
    if action == glfw.GLFW.PRESS or action == glfw.GLFW.REPEAT then
        if k == glfw.GLFW.KEY_ESCAPE then os.exit(0)
        elseif k == glfw.GLFW.KEY_SPACE then
        	rocket.send_toggle_pause(client)
        	print("Pause "..rocket.paused)
        end
    end
end

function display()
    gl.ClearColor(0,0,0,0)
    gl.Clear(GL.COLOR_BUFFER_BIT + GL.DEPTH_BUFFER_BIT)
end

function main()

	glfw.glfw.Init()

	local windowTitle = "Lua Rocket Editor"
	window = glfw.glfw.CreateWindow(800,600,windowTitle,nil,nil)
	glfw.glfw.MakeContextCurrent(window)
	glfw.glfw.SetKeyCallback(window, onkey)
	glfw.glfw.SetWindowSizeCallback(window, resize)

	init_gl()
	while glfw.glfw.WindowShouldClose(window) == 0 do
		rk.receive_and_process_command_editor(client)
		glfw.glfw.PollEvents()
		display()
		glfw.glfw.SwapBuffers(window)
	end
end

main()
