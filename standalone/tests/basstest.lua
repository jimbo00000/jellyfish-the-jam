-- basstest.lua
local bass = require("bass")
package.loadlib("socket/core.dll", "luaopen_socket_core")
local socket = require("socket.core")


print("Testing bass...")

-- #define TRUE 1
-- #define FALSE 0
local init_ret = bass.BASS_Init(-1, 44100, 0, 0, nil)
print(init_ret)


local stream = bass.BASS_StreamCreateFile(false, "tunechop.ogg", 0, 0, bass.BASS_STREAM_PRESCAN)
print("Stream: "..stream)


local start_ret = bass.BASS_Start()
print("Start: "..start_ret)


local play_ret = bass.BASS_ChannelPlay(stream, false)
print("Play: "..play_ret)

socket.sleep(2)


local sfret = bass.BASS_StreamFree(stream)
print("stream free: "..sfret)

local free_ret = bass.BASS_Free()
print(free_ret)

