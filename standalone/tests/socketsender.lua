-- socketsender.lua

package.loadlib("socket/core.dll", "luaopen_socket_core")
local socket = require("socket.core")

print(_VERSION)
print(socket._VERSION)

-- Run socketlistener.lua in a separate process
local obj = socket.tcp()
obj:settimeout(100)
-- GNU Rocket #define SYNC_DEFAULT_PORT 1338
c = obj:connect("127.0.0.1", 1338)

--obj:send("Datagram incoming!\n")

local tstart = socket.gettime()
local fnum = 0
while 1 do
    local t = socket.gettime() - tstart
    blue = math.sin(3.14159*2*t)
    obj:send(blue.."\n")
    fnum = fnum + 1
    -- Too much message traffic and the client gets backlogged
    -- only reading one message per frame
    socket.sleep(.001)
end
