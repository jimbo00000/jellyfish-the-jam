-- socketlistener.lua

local ffi = require( "ffi" )
package.loadlib("socket/core.dll", "luaopen_socket_core")
local socket = require("socket.core")

local obj = socket.tcp()
-- GNU Rocket #define SYNC_DEFAULT_PORT 1338
c = obj:bind("127.0.0.1", 1338)
obj:listen(32)
-- If other instances are running, this will fail
local client = obj:accept()
obj:settimeout(nil)

io.stdout:setvbuf("no")


while 1 do
	local s, status, r = client:receive("*l") -- get a line of text
	print("  receive: "..(s or r))
	if status == "closed" then break end
end
