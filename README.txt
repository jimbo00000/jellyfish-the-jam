     __       .__  .__           _____.__       .__         
    |__| ____ |  | |  | ___.__._/ ____\__| _____|  |__   /\ 
    |  |/ __ \|  | |  |<   |  |\   __\|  |/  ___/  |  \  \/ 
    |  \  ___/|  |_|  |_\___  | |  |  |  |\___ \|   Y  \ /\ 
/\__|  |\___  >____/____/ ____| |__|  |__/____  >___|  / \/ 
\______|    \/          \/                    \/     \/     
         __  .__                  __                
       _/  |_|  |__   ____       |__|____    _____  
       \   __\  |  \_/ __ \      |  \__  \  /     \ 
        |  | |   Y  \  ___/      |  |/ __ \|  Y Y  \
        |__| |___|  /\___  > /\__|  (____  /__|_|  /
                  \/     \/  \______|    \/      \/ 

Code: Jimbo
Music: CapsAdmin

This demo was written entirely in lua using the ffi to call into OpenGL.
The demo itself is source, and contains Win32 and Linux executables and
libraries for running it. Just double-click the batch script on Windows
or run 'cd standalone && ./luajit jellyfish_demo.lua compo' on Linux.

If you are interested in reading the source, please feel free to take a
moment to point out to me all the silly and plain wrong things I'm doing
with luajit. Some points of interest: the keyframe lookup in rocket.lua
uses a sort, the lack of any sort of prototype-based inheritance, using 
an instanced draw call for each jellyfish tentacle, etc. Maybe you can
tell me why I have to call the garbage collector manually in main!

Thanks to:

CapsAdmin, world Gmod champion. Your music rocks! - https://soundcloud.com/capsadmin
Roberto Ierusalimschy for Lua - http://www.lua.org/
Mike Pall for Luajit - http://luajit.org/
kusma, emoon for Rocket - https://github.com/emoon/rocket
daurnimator for luajit help
lpghatguy for luajit help and Coeus - https://github.com/titan-studio/coeus
malkia for ufo - https://github.com/malkia/ufo
Khronos for OpenGL - https://www.opengl.org/
metoikos and Dr. Claw for the event - http://atparty-demoscene.net/
iq - http://www.iquilezles.org/
BeautyPi - http://www.beautypi.com/
all @party attendees
